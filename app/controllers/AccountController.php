<?php

class AccountController extends BaseController {
    
    public function test(){
        $email = "john@teeshirts.com";
        $password = "admin123";
        echo md5($email.':'.md5($password));;
    }
    public function Login() {

        $email = Input::get('email');
        $password = Input::get('password');

        $accountProvider = App::make('account');
        $account = $accountProvider->Login($email, $password);
      
        if ($account) {
       
            $tokenProvider = App::make('token');
            $token = $tokenProvider->Generate($account->id);

            return Response::json(array(
                        'error'    => 0,
                        'message'   => 'Login successful!',
                        'data'      => array(
                            'token' => $token
                        )), 200, [], JSON_NUMERIC_CHECK);
        } else {
            return Response::json(array(
                        'error' => 1,
                        'message' => 'Failed to authenticate user.',
                            ), 200, [], JSON_NUMERIC_CHECK);
        }
       
    }

    public function GetCurrentUser(){

        $token              = Input::get('token');
        $accountProvider    = App::make('account');
        $account            = $accountProvider->GetAccountByToken($token);

        if($account){
            return Response::json(array(
                'error'    => 0,
                'message'   => 'Login successful!',
                'data'      => array(
                    'account' => $account
                )), 200, [], JSON_NUMERIC_CHECK);
        }else{
            return Response::json(array(
                    'error' => 1,
                    'message' => 'user not identified'
                        ), 200, [], JSON_NUMERIC_CHECK);
        }

    }
    
    
        
}