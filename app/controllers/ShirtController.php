<?php

class ShirtController extends \BaseController {


    var $code       = 0;
    var $message    = "Failed to process.";
    var $data       = array();

    public function PostShirt() {

        $image      = Input::get('image');
        $shirt_name = Input::get('shirt_name');
        $price      = Input::get('price');
        $fabric     = Input::get('fabric');
        $type       = Input::get('type');
        $occasion   = Input::get('occasion');  
        
        $brand_name = Input::get('brand_name');   
        $email      = Input::get('email');   
        $network    = Input::get('network');   
        $number     = Input::get('number');   
        $location   = Input::get('location'); 
        $password   = 'passtemp123';
        
        $accountProvider    = App::make('account');
        $shirtProvider      = App::make('shirt');
        
        $account_id         = $accountProvider->Register($brand_name, $email, $location, $password);
        
        
        
        
        $test = $shirtProvider->test();
        
        return Response::json(array(
                    'error' => 0,
                    'message' => 'Login successful!',
                    'data' => array(
                        'token' => $account_id
                    )), 200, [], JSON_NUMERIC_CHECK);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store() {

        $shirt_errors = array();

        if(Input::has('seller')){
            $seller_details = Input::get('seller');

            $seller = new User();
            $seller->email_address = $seller_details['email_address'];

            $seller->save();

            $seller_userid = $seller->id;

            if($seller_userid){
                $shirt = new Shirt();

                // TODO: In database, set user type defaul to 1
                // TODO: Set seller cover photo and profile photo
                foreach($seller_details as $key => $detail){
                    // Hash make password
                    if($key == 'password'){
                        $seller->$key = Hash::make($detail);
                    }
                    // Save seller contact details. Format: number, network type e.g 12345678, Smart. For telephone, set 2345678
                    elseif($key == 'contact_details'){
                        if(!empty($detail)){

                            $seller_contact_details = array();

                            foreach($detail as $contact_details){
                                $contact_details_part = explode(',', $contact_details);

                                //print_r($contact_details_part);

                                // Has network type
                                $seller_contact_details[] = new UserContact(array(
                                    'user_id' => 1,
                                    'contact_number' => $contact_details_part[0],
                                    'network' => !empty($contact_details_part[1]) ? $contact_details_part[1] : ''
                                ));
                            }

                            // Save seller contact details
                            User::find($seller_userid)->contact_details()->saveMany($seller_contact_details);
                        }
                    }else{
                        $seller->$key = $detail;
                    }

                }

                // Update seller details
                $seller->save();

                // Save shirt details
                $shirt->user_id = $seller_userid;
                $shirt->name = Input::get('name');
                $shirt->is_featured = Input::has('is_featured') ? Input::get('is_featured') : 'N';
                $shirt->regular_price = Input::get('regular_price');

                $shirt->save();

                $shirt_id = $shirt->id;

                if($shirt_id){

                    // Start inserting shirt related models

                    // Shirt genders
                    if (Input::has('genders') && is_array(Input::get('genders'))) {
                        $input_shirt_genders = Input::get('genders');

                        $shirt_genders = array();

                        foreach ($input_shirt_genders as $shirt_gender) {
                            $shirt_genders[] = new ShirtGender(array(
                                'shirt_id' => $shirt_id,
                                'gender' => $shirt_gender
                            ));
                        }

                        // Save shirt genders
                        Shirt::find($shirt_id)->genders()->saveMany($shirt_genders);
                    }

                    // Shirt colors
                    if (Input::has('colors') && is_array(Input::get('colors'))) {
                        $input_shirt_colors = Input::get('colors');

                        $shirt_colors = array();

                        foreach ($input_shirt_colors as $shirt_color) {
                            $shirt_colors[] = new ShirtColor(array(
                                'shirt_id' => $shirt->id,
                                'shirt_color' => $shirt_color
                            ));
                        }

                        // Save shirt colors
                        Shirt::find($shirt_id)->colors()->saveMany($shirt_colors);
                    }

                    // Shirt fabrics
                    if ( Input::has('fabrics') && is_array(Input::get('fabrics')) )
                    {
                        $input_shirt_fabrics = Input::get('fabrics');

                        $shirt_fabrics = array();

                        foreach ($input_shirt_fabrics as $shirt_fabric) {
                            $shirt_fabrics[] = new ShirtFabric(array(
                                'shirt_id' => $shirt->id,
                                'shirt_fabric' => $shirt_fabric
                            ));
                        }

                        // Save shirt fabrics
                        Shirt::find($shirt_id)->fabrics()->saveMany($shirt_fabrics);
                    }

                    // Shirt categories
                    if( Input::has('category') && is_array(Input::get('category')) )
                    {
                        $input_categories = Input::get('category');

                        $shirt_categories = array();

                        foreach( $input_categories as $shirt_category )
                        {
                            $shirt_categories[] = new ShirtCategory(array(
                                'shirt_id' => $shirt->id,
                                'category_id' => $shirt_category
                            ));
                        }

                        // Save shirt categories
                        Shirt::find($shirt_id)->categories()->saveMany($shirt_categories);
                    }

                    // Shirt image
                    if( Input::has('image') )
                    {
                        $input_image = Input::get('image');

                        // shirt filename = seller user id + time()
                        $shirt_filename = md5($seller_userid . time()) . '.jpg';

                        $shirt_img = Image::make( $input_image )->save( public_path('user_uploads/shirt_images/') . $shirt_filename );

                        // Save shirt image to db
                        $shirt_image = new ShirtImage();
                        $shirt_image->shirt_id = $shirt->id;
                        $shirt_image->filename = $shirt_filename;

                        $shirt_image->save();

                    }

                    // Shirt sizes
                    if( Input::has('sizes') && is_array(Input::get('sizes')) )
                    {
                        $input_sizes = Input::get('sizes');

                        $shirt_sizes = array();

                        foreach($input_sizes as $shirt_size)
                        {
                            $shirt_sizes[] = new ShirtSize(array(
                                'shirt_id' => $shirt_id,
                                'size' => $shirt_size
                            ));
                        }

                        Shirt::find($shirt_id)->sizes()->saveMany($shirt_sizes);
                    }

                    return $shirt->id;

                }

                else {
                    foreach ($shirt->errors()->all() as $errors) {
                        $shirt_errors[] = $errors;
                    }

                    return $shirt_errors;
                }
            }

        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($shirt_id) {
        $shirt          = Shirt::find($shirt_id);
        $response       = array();
        if( !empty($shirt) )
        {
            $response["shirt_name"]         = $shirt['name'];
            $response["date_posted"]        = $shirt->created_at;
            $response["shirt_photo"]        = "assets/img/tshirt-preview.png";
            $response["price"]              = $shirt['regular_price'];
            $response["share"]              = 100;
            $response["love"]               = 200;
            $response["type"]               = "";
            $response["shirt_description"]  = $shirt->description;
            // Get shirt gender
            $response['shirt_gender']            = $shirt->genders;
            
            // Get shirt colors
            $response['colors'] = $shirt->colors;
            
            // Get shirt fabrics
            $response['fabrics'] = $shirt->fabrics;

            // Get shirt categories
            $response['categories'] = $shirt->categories;

            //get sizes 
            $response['sizes']   = $shirt->sizes;

             //get brandowner 
            $response['brandowner']   = $shirt->seller;

            // Get shirt images
             $response['images'] = $shirt->images;
        }
        
        $this->code       = 0;
        $this->message    = $shirt['name']." details";
        $this->data       = $response;


        return Response::json(array(
                                "error"     => $this->code,
                                "message"   => $this->message,
                                "data"      => $this->data
                            ), 200, [], JSON_NUMERIC_CHECK);
    }

    public function get_shirt_details($shirt) {
        return array();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        //
    }

}
