<?php

class ShirtsController extends \BaseController {

    public $shirts = array();
    
    public $per_page = 2;
    
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $shirts = array();

        // Shirt category
        if( Input::has('category') )
        {
        	$shirts = DB::table('tee_shirts')
			->leftJoin('tee_shirt_categories', function($join){
				$join->on('tee_shirts.id', '=', 'tee_shirt_categories.shirt_id');
			})
			->where('tee_shirt_categories.category_id', '=', Input::get('category'))
			->where('tee_shirts.is_active', '=', 'y')
			->select('tee_shirts.*')
			->paginate( $this->per_page );

			if( !empty($shirts) )
			{
				foreach($shirts as $shirt)
				{
					// Need to define property for DB Queries result
					
					// Get shirt gender
	                $shirt->genders = Shirt::find($shirt->id)->genders;

					// Shirt colors
					$shirt->colors = Shirt::find($shirt->id)->colors;
	                
	                // Get shirt fabrics
	                $shirt->fabrics = Shirt::find($shirt->id)->fabrics;

	                // Get shirt categories
	                $shirt->categories = Shirt::find($shirt->id)->categories;

                    // Get shirt sizes
                    // $shirt->sizes = Shirt::find($shirt->id)->sizes;

	                // Get shirt seller
	                $shirt->seller = Shirt::find($shirt->id)->seller;
				}
			}

			return $shirts;
        }
        else
        {
        	$shirts = Shirt::where('is_active', '=', 'y');
        	
        	// Get query parameters
	        if( Input::has('featured') )
	        {
	            $input_is_featured = strtolower(Input::get('featured'));
	            $input_is_featured = $input_is_featured == 'y' ? 'Y' : 'N';
	            
	            $shirt = $shirts->where('is_featured', '=', $input_is_featured);
	        }
	        
	        // Order by
	        if( Input::has('orderby') && Input::has('order') )
	        {
	            // echo 'has orderby and order params';
	            $input_order = !empty( Input::get('order') ) ? Input::get('order') : 'desc';
	            $input_order = strtolower($input_order);
	            
	            $shirts = $shirts->orderBy( Input::get('orderby'), $input_order );
	        }

	        $shirts = $shirts->paginate( $this->per_page );

	        if( !empty($shirts) )
	        {
	            foreach( $shirts as $shirt )
	            {
	                // Get shirt gender
	                $shirt->genders = $shirt->genders;
	                
	                // Get shirt colors
	                $shirt->colors = $shirt->colors;
	                
	                // Get shirt fabrics
	                $shirt->fabrics = $shirt->fabrics;

	                // Get shirt categories
	                $shirt->categories = $shirt->categories;

                    // Get shirt sizes
                    $shirt->sizes = $shirt->sizes;

	                // Get shirt images
	                $shirt->images = $shirt->images;
	            }
	        }

	        return $shirts;
        }
        
	}
    
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
