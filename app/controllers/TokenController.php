<?php

class TokenController extends BaseController {

    public function Check() {
        $token = Input::get('token');
        if ($token) {
            //  verify token
            $generator = App::make('token');
            $valid = $generator->Check($token);
            if (!$valid) {
                return Response::json(array(
                            'error' => 1,
                            'message' => 'Failed to authenticate token'
                                ), 200, [], JSON_NUMERIC_CHECK);
            }
        } else {
            return Response::json(array(
                        'error' => 1,
                        'message' => 'No token available'
                            ), 200, [], JSON_NUMERIC_CHECK);
        }
    }

}
