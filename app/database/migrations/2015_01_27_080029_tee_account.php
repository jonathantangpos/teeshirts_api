<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TeeAccount extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('tee_account', function($table){
		    $table->increments('id');
		    $table->string('brand_name',255);
		    $table->string('email_address',50);
		    $table->string('location',255);
		    $table->string('password',255);
		    $table->string('user_type',255);
		    $table->text('profile_photo');
		    $table->text('cover_photo');
		    $table->text('description');
		    $table->string('gender',50);
		    $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
