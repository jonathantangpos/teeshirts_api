<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTeeShirtsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tee_shirts', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('user_id')->default(0);
			$table->string('name')->default('0');
			$table->string('is_active')->nullable()->default('N');
			$table->string('is_featured')->nullable()->default('N');
			$table->string('fb_post_id')->nullable();
			$table->integer('regular_price')->nullable()->default(0);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tee_shirts');
	}

}
