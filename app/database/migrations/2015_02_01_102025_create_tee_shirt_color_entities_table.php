<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTeeShirtColorEntitiesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tee_shirt_color_entities', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('shirt_id')->default(0);
			$table->string('shirt_color', 50)->default('0');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tee_shirt_color_entities');
	}

}
