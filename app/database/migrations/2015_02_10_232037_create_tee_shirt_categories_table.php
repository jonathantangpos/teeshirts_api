<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTeeShirtCategoriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tee_shirt_categories', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('shirt_id')->default(0);
			$table->integer('category_id')->default(0);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tee_shirt_categories');
	}

}
