<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class UpdateTeeShirtFabricEntitiesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		/**
		 * Change shift_fabric column type to varchar
		 * @param  )      $table [description]
		 * @return [type]        [description]
		 * see; http://blog.koomai.net/post/updating-column-types-with-laravel's-schema-builder
		 */
		DB::statement('ALTER TABLE `tee_shirt_fabric_entities` CHANGE COLUMN `shirt_fabric` `shirt_fabric` VARCHAR(50) NOT NULL AFTER `shirt_id`;');
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		DB::statement('ALTER TABLE `tee_shirt_fabric_entities` CHANGE COLUMN `shirt_fabric` `shirt_fabric` INT NOT NULL DEFAULT \'0\' COLLATE \'utf8_unicode_ci\' AFTER `shirt_id`;')	;
	}

}
