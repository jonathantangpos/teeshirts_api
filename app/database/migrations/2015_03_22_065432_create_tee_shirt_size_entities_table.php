<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTeeShirtSizeEntitiesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tee_shirt_size_entities', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('shirt_id')->default(0);
			$table->integer('size')->default(0);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tee_shirt_size_entities');
	}

}
