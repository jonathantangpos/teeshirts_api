<?php

class TeeAccountContactTableSeeder extends Seeder {

	/**
	 * Auto generated seed file
	 *
	 * @return void
	 */
	public function run()
	{
		\DB::table('tee_account_contact')->delete();
        
		\DB::table('tee_account_contact')->insert(array (
			0 => 
			array (
				'id' => 1,
				'user_id' => 1,
				'contact_number' => 2345678,
				'network' => 'Smart',
				'created_at' => '0000-00-00 00:00:00',
				'updated_at' => '0000-00-00 00:00:00',
			),
			1 => 
			array (
				'id' => 2,
				'user_id' => 1,
				'contact_number' => 234567,
				'network' => 'Smart',
				'created_at' => '2015-03-15 06:47:07',
				'updated_at' => '2015-03-15 06:47:07',
			),
			2 => 
			array (
				'id' => 3,
				'user_id' => 1,
				'contact_number' => 234567,
				'network' => 'Smart',
				'created_at' => '2015-03-15 06:48:32',
				'updated_at' => '2015-03-15 06:48:32',
			),
		));
	}

}
