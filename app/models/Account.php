<?php

namespace Models;
use Eloquent;

class Account extends BaseModel {

    protected $table = 'tee_account';

    protected $hidden = array('id');

    protected $fillable = array('brand_name', 'email_address', 'location','password',
        'user_type','profile_photo','cover_photo','description','gender');

}
