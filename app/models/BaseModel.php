<?php

use \LaravelBook\Ardent\Ardent;

class BaseModel extends Ardent {
	
    public $timestamps = false;
    
}

