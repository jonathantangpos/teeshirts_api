<?php

class Shirt extends BaseModel {
	protected $table = 'tee_shirts';
    
    public $timestamps = true;

    protected $fillable = array( 'user_id', 'name', 'is_featured', 'regular_price', 'is_active', 'fb_post_id' );
    
    public static $rules = array(
        'user_id' => 'required',
        'name' => 'required',
        'regular_price' => 'required',
    );

    public function genders()
	{
        return $this->hasMany('ShirtGender', 'shirt_id', 'id');
	}
    
    public function colors()
    {
        return $this->hasMany('ShirtColor', 'shirt_id', 'id');
    }
    
    public function fabrics()
    {
        return $this->hasMany('ShirtFabric', 'shirt_id', 'id');
    }

    public function categories()
    {
        return $this->hasMany('ShirtCategory', 'shirt_id', 'id');
    }
    public function sizes()
    {
         return $this->hasMany('ShirtSize', 'shirt_id', 'id');
    }
    public function seller()
    {
        return $this->hasOne('User', 'id', 'user_id');
    }

    public function images()
    {
        return $this->hasMany('ShirtImage', 'shirt_id', 'id');
    }
        
}