<?php

class ShirtBrandOwner extends BaseModel {

	protected $table = 'tee_account';
    
    public $timestamps = true;
    
    protected $fillable = array('user_id', 'id');
    
    public static $rules = array(
        'shirt_id' => 'required',
        'size_id' => 'required',
    );

}