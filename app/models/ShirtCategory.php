<?php

class ShirtCategory extends BaseModel {

	protected $table = 'tee_shirt_categories';

	public $timestamps = true;

	protected $fillable = array('shirt_id', 'category_id');

	public static $rules = array(
			'shirt_id' => 'required',
			'category_id' => 'required',
		);

}