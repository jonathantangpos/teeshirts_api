<?php

class ShirtColor extends BaseModel {
    
    protected $table = 'tee_shirt_color_entities';
    
    public $timestamps = true;
    
    protected $fillable = array( 'shirt_id', 'shirt_color' );
    
    public static $rules = array(
        'shirt_id' => 'required',
        'shirt_color' => 'required',
        
    );
    
}
