<?php

class ShirtFabric extends BaseModel {
    
    protected $table = 'tee_shirt_fabric_entities';
    
    public $timestamps = true;
    
    protected $fillable = array('shirt_id', 'shirt_fabric');
    
    public static $rules = array(
        'shirt_id' => 'required',
        'shirt_fabric' => 'required',
    );
    
}

