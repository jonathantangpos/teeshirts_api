<?php

class ShirtGender extends BaseModel {

	protected $table = 'tee_shirt_genders';
    
    public $timestamps = true;
    
    protected $fillable = array('shirt_id', 'gender');
    
    public static $rules = array(
        'shirt_id' => 'required',
        'gender' => 'required',
    );

}