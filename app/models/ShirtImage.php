<?php

class ShirtImage extends BaseModel {

	protected $table = 'tee_shirt_images';

	protected $fillable = array('shirt_id', 'filename');

	public static $rules = array(
			'shirt_id' => 'required',
			'filename' => 'required'
		);



}