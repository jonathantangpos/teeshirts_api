<?php

class ShirtSize extends BaseModel {

	protected $table = 'tee_shirt_size_entities';
    
  public $timestamps = true;
  
  protected $fillable = array('shirt_id', 'size');
  
  public static $rules = array(
      'shirt_id' => 'required',
      'size' => 'required',
  );
}