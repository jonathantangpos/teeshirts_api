<?php

namespace Models;
use Eloquent;
class Token extends Eloquent {

    protected $table = 'tee_token';
    protected $hidden = array('id');
    protected $fillable = array('key', 'user_id', 'expiration');

}
