<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends BaseModel implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'tee_account';

    public $timestamps = true;

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');

    protected $fillable = array('brand_name', 'email_address', 'location','password',
        'user_type','profile_photo','cover_photo','description','gender');

	public function shirts()
	{
		return $this->hasMany('Shirt', 'user_id', 'id');
	}

    public function contact_details(){
        return $this->hasMany('UserContact', 'user_id', 'id');
    }


}
