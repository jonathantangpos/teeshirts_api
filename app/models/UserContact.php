<?php

class UserContact extends BaseModel
{
	protected $table = 'tee_account_contact';

	public $timestamps = true;

	protected $fillable = array('user_id', 'contact_number', 'network');

	public static $rules = array(
		'user_id' => 'required',
		'contact_number' => 'required',
		);
}