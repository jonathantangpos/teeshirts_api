<?php

namespace Providers;

use Illuminate\Support\ServiceProvider;
use Services\AccountService;

class AccountProvider extends ServiceProvider {

    public function register() {

        $this->app->bind('account', function($app) {
            return new AccountService();
        });
        
    }

}