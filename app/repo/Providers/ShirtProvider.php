<?php

namespace Providers;

use Illuminate\Support\ServiceProvider;
use Services\ShirtService;

class ShirtProvider extends ServiceProvider {

    public function register() {

        $this->app->bind('shirt', function($app) {
            return new ShirtService();
        });
        
    }

}