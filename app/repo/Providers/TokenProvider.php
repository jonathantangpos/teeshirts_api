<?php

namespace Providers;

use Illuminate\Support\ServiceProvider;
use Services\TokenService;

class TokenProvider extends ServiceProvider {

    public function register() {

        $this->app->bind('token', function($app) {
            return new TokenService();
        });
        
    }

}