<?php

namespace Services;

use Illuminate\Support\Facades\Hash;
use Models\Account;
use Models\Token;

class AccountService {

    public function GetAccountById($id){
        $user           =   Account::where('id','=',$id);
        $data           =   $user->first();
        return $data;
    }

    public function GetAccountByToken($key){
        $token      =   Token::where('key','=',  $key);
        $accountToken  = $token->first();
        if($accountToken){
            return $this->GetAccountById($accountToken->user_id);
        }
    }
    
    public function Register($brand_name, $email, $location, $password){
        $account                = new Account();
        $account->brand_name    = $brand_name;
        $account->email_address = $email;
        $account->location      = $location;
        $account->password      = $this->HashPassword($email, $password);
        $account->save();
        
        return $account->id;
    }

    public function Login($email, $password){
        $user   =   Account::where('email_address','=',$email)->where('password','=',  $this->HashPassword($email, $password));
        return $user->first();
    }

    public function HashPassword($email,$password){
        return md5($email.':'.md5($password));
    }

}
