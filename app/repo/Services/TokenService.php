<?php

namespace Services;

use Illuminate\Support\Facades\Hash;
use Models\Account;
use Models\Token;

class TokenService {

    public function Generate($id) {
        $currentDate        =   date('Y-m-d H:i:s');
        $key                =   Hash::make('user:'.$id);
        $token              =   new Token();
        $token->key         =   $key;
        $token->expiration  =   date('Y-m-d H:i:s', strtotime("$currentDate + 4 hours"));
        $token->user_id     =   $id;
        $token->save();
        return $key;
    }

    public function check($token) {
        try {
            $dbToken        =   Token::where('key', '=', urldecode($token))->first();
            if ($dbToken) {
                $dbUser                 =   Account::where('id', '=', $dbToken->user_id)->first();
                $currentTimeStamp       =   strtotime(date('Y-m-d H:i:s'));
                $expirationTimeStamp    =   strtotime($dbToken->expiration);
                return ($dbUser && $currentTimeStamp < $expirationTimeStamp);
            } else {
                return false;
            }
        } catch (Exception $e) {
            return false;
        }
    }

}
