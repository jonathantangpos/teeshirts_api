<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('hello');
});

/**
 * REST API for Shirt
 */
Route::resource('shirt', 'ShirtController');

/**
 * REST API for Shirts
 */
Route::resource('shirts', 'ShirtsController');
Route::get('test', 'AccountController@test');
Route::post('/login', 'AccountController@Login');
Route::post('/post', 'ShirtController@PostShirt');

Route::get('/me', array(
    'before' => 'api.authenticate',
    'uses' => 'AccountController@GetCurrentUser'
));

Route::get('testalfie', function(){
	
    $user_contact =  User::find(1)->contact_details;
    dd(DB::getQueryLog());

    return $user_contact;
    //return UserContact::find(1);
    //return User::find(1);

    /*$usercontact = new UserContact(array(
        'user_id' => 1,
        'contact_number' => '234567',
        'network' => 'Smart'
    ));

    return User::find(1)->contact_details()->save($usercontact);*/
});

// Display all SQL executed in Eloquent
/*Event::listen('illuminate.query', function($query)
{
    var_dump($query);
});*/

Route::post('uploadimage', function(){
	$input_image = Input::get('image');

	$img = Image::make( $input_image );

	$img->save( public_path('user_uploads/shirt_images/jennifer.jpg') );

});

Route::get('user', function(){
	// return User::find(1);
	return Shirt::find(1)->seller;
});

Route::get('shirtimage', function(){

	/*
	$shirt_image = new ShirtImage();

	$shirt_image->shirt_id = '1';
	$shirt_image->filename = 'sgasfisafipsf12345678901212';

	$shirt_image->save();
	*/
	return Shirt::find(1)->images;

});

