-- phpMyAdmin SQL Dump
-- version 4.2.9.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 28, 2015 at 06:11 PM
-- Server version: 5.5.40-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `teeshirts_v001`
--

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2015_01_26_125958_tee_token', 1),
('2015_01_27_080029_tee_account', 2);

-- --------------------------------------------------------

--
-- Table structure for table `tee_account`
--

CREATE TABLE IF NOT EXISTS `tee_account` (
`id` int(10) unsigned NOT NULL,
  `brand_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_address` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `profile_photo` text COLLATE utf8_unicode_ci NOT NULL,
  `cover_photo` text COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `gender` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tee_account`
--

INSERT INTO `tee_account` (`id`, `brand_name`, `email_address`, `location`, `password`, `user_type`, `profile_photo`, `cover_photo`, `description`, `gender`, `created_at`, `updated_at`) VALUES
(1, 'Ohlala', 'john@admin.com', '', '8c575bae21b26b43731b72ba34509f64', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tee_token`
--

CREATE TABLE IF NOT EXISTS `tee_token` (
`id` int(10) unsigned NOT NULL,
  `key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `expiration` datetime NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tee_token`
--

INSERT INTO `tee_token` (`id`, `key`, `user_id`, `expiration`, `created_at`, `updated_at`) VALUES
(1, '$2y$10$lVEBd78udWMvn/rmM.Hy9.f4Ut5oBNGemvctNeYF9o.g0fee.1ugS', 1, '2015-01-27 13:24:55', '2015-01-27 01:24:55', '2015-01-27 01:24:55'),
(2, '$2y$10$WAJ.umTo4u2gv1MVDmXQfehtapzKUjmUVK.uCFvR9R2FZLxqUMQSi', 1, '2015-01-27 17:13:26', '2015-01-27 05:13:26', '2015-01-27 05:13:26'),
(3, '$2y$10$mMTC2fE1jEF5iK9dkeDnSObwPr.dtdpe93b3TkAVZDg0rtqgX3N3G', 1, '2015-01-27 17:15:56', '2015-01-27 05:15:56', '2015-01-27 05:15:56'),
(4, '$2y$10$uCV2.Rgxz6.PPVmqooQNxeReGOwB8Sr64h70gP8Hi.92XyiRMlLue', 1, '2015-01-27 17:56:38', '2015-01-27 05:56:38', '2015-01-27 05:56:38'),
(5, '$2y$10$rnft9Kogj1DWiZ89HJjgmugi6GcL2zP2UUEpFpk6PriQCApTge1.e', 1, '2015-01-27 18:16:37', '2015-01-27 06:16:37', '2015-01-27 06:16:37'),
(6, '$2y$10$9tTI5KjiRNAdJfaf.FVZJ.u37XsIVPDMasDypCzfI1AxEx8pmH0Fa', 1, '2015-01-27 18:17:11', '2015-01-27 06:17:11', '2015-01-27 06:17:11'),
(7, '$2y$10$y/bvgyM4xOKtb94w3bB1JuDfYpY.JwjKX5xv6Z31QD9x0dKO9nLrW', 1, '2015-01-27 18:18:23', '2015-01-27 06:18:23', '2015-01-27 06:18:23'),
(8, '$2y$10$er8ATDvN6UPXfwLT/quZZe.C/V341o4TA.c1bmnG.n0pHyP9vNo8q', 1, '2015-01-27 19:12:27', '2015-01-27 07:12:27', '2015-01-27 07:12:27'),
(9, '$2y$10$qb1V8aaOxXMcODgxLx2beuQypoWFchHV0wy1KkBFUWlI0KzoEvzdG', 1, '2015-01-28 06:46:13', '2015-01-27 18:46:13', '2015-01-27 18:46:13'),
(10, '$2y$10$kd6QIkdV6rOiNxsCdPqrB.uw9mBk5MNx5KY2xVtpMdfPsMmCbtu9e', 1, '2015-01-28 13:54:27', '2015-01-28 01:54:27', '2015-01-28 01:54:27'),
(11, '$2y$10$oqjWJ7va3hMC7gzBvVJs6OXV6AmZ7qIlJsqUNP05yzqMFyjlKt1kS', 1, '2015-01-28 13:56:04', '2015-01-28 01:56:04', '2015-01-28 01:56:04'),
(12, '$2y$10$qHf4PEeGntU4JagdaNDbuO1xLiT5s1KRzBwHgp.cqrhWX.n5vx8bG', 1, '2015-01-28 13:57:09', '2015-01-28 01:57:09', '2015-01-28 01:57:09'),
(13, '$2y$10$C5YaqpckRzn7j9PGlusPhu/U3Ly9Fe7cyu07tdEHrQqj9yIOU4arG', 1, '2015-01-28 13:57:44', '2015-01-28 01:57:44', '2015-01-28 01:57:44');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tee_account`
--
ALTER TABLE `tee_account`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tee_token`
--
ALTER TABLE `tee_token`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tee_account`
--
ALTER TABLE `tee_account`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tee_token`
--
ALTER TABLE `tee_token`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
